


#################### Detect custom jython libs ######################
import sys, os
_prefs = Prefs(); _path = _prefs.get("JythonExtra.path", "")
#if _path == "" or not os.path.exists(_path):
if _path == "" or not os.path.exists(_path):
        _path = IJ.getDirectory("Please select 'jython-libs'")[:-1]
_prefs.set("JythonExtra.path", _path); _prefs.savePreferences()
sys.path.append(_path)
#####################################################################


import os, shutil, threading, time
import ij.plugin.frame.RoiManager
import ij.gui.Roi
import ij.plugin.ImageCalculator
import ij.io.DirectoryChooser
import ij.gui.NonBlockingGenericDialog
import ij.Prefs
import math
import ij.gui.Plot
import time
from ij import IJ  

# my imports
from segmentNucleus import segmentNucleus # useless but would require a few changes to get rid of it
from openAndCrop import openAndCrop
from segmentLocus import segmentLocus

PREFS = Prefs()

################## FIXED PARAMETERS ##################
CONFIG = {}

# PROGRAM VERSION
CONFIG["VERSION"] = "0p01"

# COMPUTER CAPACITIES
CONFIG["NCPUS"] = PREFS.get("HerbHenr_tracker.ncpus", 8)
CONFIG["ENABLE_THREADING"] = bool(PREFS.get("HerbHenr_tracker.threading", 0))

# TIF FILES LOADING PARAMETERS
CONFIG["BASE_IMAGE_START"] = 0
CONFIG["BASE_IMAGE_END"] = PREFS.get("HerbHenr_tracker.base_image_end", 9999)
CONFIG["NBRE_CHANNELS"] = PREFS.get("HerbHenr_tracker.nbre_channels", 1)

# NUCLEUS SEGMENTATION PARAMETERS
CONFIG["NUCLEUS_SNR"] = 10  # threshold between bck and nucleoplasm

# CENTROID CALCULATION PARAMETERS
CONFIG["SMOOTHED"] = 0
CONFIG["MASS_CENTER"] = 1
CONFIG["CENTROID_METHOD"] = "gaussian fitting"

# LOCUS SEGMENTATION PARAMETERS
CONFIG["DO3D"] = 0

# COLOR MERGE
CONFIG["MAGN"] = 5
CONFIG["Seuil_Iratio"] = 1.5
CONFIG["Seuil_SNR_max"] = 5
CONFIG["Seuil_SNR_mean"] = 1.5

# SAVE ANALYSED IMAGES
CONFIG["saveGraphicalOutput"] = bool(PREFS.get("HerbHenr_tracker.saveGraphicalOutput", 0))

# NEED FOR THREADING TO WORK
CONFIG["MAIN_LOCK"] = threading.Lock()

# NEED FOR SPATIAL ANALYSIS
# CONFIG["pixelSize"] =  108 # nm
CONFIG["pixelSize"] = PREFS.get("HerbHenr_tracker.pixelSize", 108) 
#PSF_rad = math.floor(300/pixelSize)
CONFIG["PSF_rad"] = 3

################## FIXED PARAMETERS ##################


gd = NonBlockingGenericDialog("Analysis preferences...")
gd.addMessage("HerbHen tracker V"+CONFIG["VERSION"])

gd.addMessage("Computer capacities") # Computer capacities
gd.addNumericField("Number of CPU : ",CONFIG["NCPUS"],0)
gd.addCheckbox("Enable threading", CONFIG["ENABLE_THREADING"])

gd.addMessage("tif files loading parameters") # tif files loading parameters
gd.addNumericField("First frame to analyse :", CONFIG["BASE_IMAGE_START"], 0)
gd.addNumericField("Last frame to analyse :", CONFIG["BASE_IMAGE_END"], 0)
gd.addNumericField("Number of channels : ",CONFIG["NBRE_CHANNELS"],0) # Number of channels of fluorescence

gd.addMessage("Nucleus segmentation parameters") # Nucleus segmentation parameters
gd.addNumericField("Nucleoplasm vs Noise SNR :", CONFIG["NUCLEUS_SNR"], 0)

gd.addMessage("Locus segmentation parameters")# Locus segmentation parameters
gd.addCheckbox("smooth image", CONFIG["SMOOTHED"])
gd.addChoice("Centroid calculation",["center of mass","gaussian fitting"],CONFIG["CENTROID_METHOD"])

gd.addMessage("Experimental settings")
gd.addNumericField("Pixel size XY (nm) : ", CONFIG["pixelSize"],0)

gd.addMessage("Color merge witness tresholds settings")
gd.addNumericField("Threshold for Iratio : ", CONFIG["Seuil_Iratio"],1)
gd.addNumericField("Threshold for SNR max : ", CONFIG["Seuil_SNR_max"],1)
gd.addNumericField("Threshold for SNR mean : ", CONFIG["Seuil_SNR_mean"],1)

gd.addCheckbox("Save analysed images in separate files", CONFIG["saveGraphicalOutput"])

gd.showDialog()
if (gd.wasCanceled()): raise Exception("Dialog canceled...")

# Computer capacities
CONFIG["NCPUS"] = int(gd.getNextNumber())
PREFS.set("HerbHenr_tracker.ncpus", CONFIG["NCPUS"])
CONFIG["ENABLE_THREADING"] = gd.getNextBoolean()
PREFS.set("HerbHenr_tracker.threading", CONFIG["ENABLE_THREADING"])

# tif files loading parameters
CONFIG["BASE_IMAGE_START"] = int(gd.getNextNumber())
CONFIG["BASE_IMAGE_END"] = int(gd.getNextNumber())
PREFS.set("HerbHenr_tracker.base_image_end", CONFIG["BASE_IMAGE_END"])
CONFIG["NBRE_CHANNELS"] = int(gd.getNextNumber())
PREFS.set("HerbHenr_tracker.nbre_channels", CONFIG["NBRE_CHANNELS"])

# Nucleus segmentation parameters
CONFIG["NUCLEUS_SNR"] = int(gd.getNextNumber())

# Locus segmentation parameters
CONFIG["SMOOTHED"] = gd.getNextBoolean()
CONFIG["CENTROID_METHOD"] = gd.getNextChoice()

# Experimental settings
CONFIG["pixelSize"] = int(gd.getNextNumber()) # nm
PREFS.set("HerbHenr_tracker.pixelSize", CONFIG["pixelSize"])

# Color merge witness tresholds settings
CONFIG["Seuil_Iratio"] = int(gd.getNextNumber())
CONFIG["Seuil_SNR_max"] = int(gd.getNextNumber())
CONFIG["Seuil_SNR_mean"] = int(gd.getNextNumber())

#Save analysed images
CONFIG["saveGraphicalOutput"] = gd.getNextBoolean()
PREFS.set("HerbHenr_tracker.saveGraphicalOutput", CONFIG["saveGraphicalOutput"])

PREFS.savePreferences()


################## FUNCTIONS ##################


def loadImage(channel, CONFIG):
	IJ.run("Close All")

	# load up image
	if os.path.isdir(CONFIG["BASE_IMAGE"]):
		IJ.run("Image Sequence...", "open=["+CONFIG["BASE_IMAGE"]+"] number="+str(CONFIG["BASE_IMAGE_NFRAMES"])+" starting="+str(CONFIG["BASE_IMAGE_START"])+" increment=1 scale=100 file=.tif or=[] sort")
		imp = IJ.getImage() #grab reference to the image
		CONFIG["ROI_FILE"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"])
	else:
		print CONFIG["BASE_IMAGE"]
		#imp = IJ.openImage(CONFIG["BASE_IMAGE"]);
		IJ.open(CONFIG["BASE_IMAGE"]);
		imp = IJ.getImage() #grab reference to the image
		CONFIG["ROI_FILE"] = CONFIG["BASE_PATH"]

	if imp is None:  
      		print "Could not open image from file:", CONFIG["BASE_IMAGE"]
      		return  

	#imp.getNSlices()
	#IJ.run("Stack to Hyperstack...", "order=xyczt(default) channels=2 slices=35 frames=1 display=Color")
	#IJ.run("Stack to Hyperstack...", "order=xyczt(default) channels="+str(CONFIG["NBRE_CHANNELS"])+" slices="+str(imp.getNSlices()/CONFIG["NBRE_CHANNELS"])+" frames=1 display=Color")
	#imp = IJ.getImage() #grab reference to the image
	IJ.run("Z Project...", "start=1 stop="+str(imp.getNSlices())+" projection=[Max Intensity]")
	imp = IJ.getImage() #grab reference to the image
	image_name = imp.getTitle()
	IJ.run("Duplicate...", "title="+str(imp.getTitle)+" channels="+str(channel+1))
	imp = IJ.getImage() #grab reference to the image
	imp.setTitle(image_name[:-4]+"channel"+str(channel+1)+".tif")

	return imp

def cropROIs(imp,CONFIG,channel):
	# load up ROIs
	roim = RoiManager(1)
	if roim.getCount()>0:
		roim.runCommand("Select All")
		roim.runCommand("Delete")

	imp_roi_list = []
	roi_positions = []
	
	# clear out results and rois if already exists
	if os.path.exists(os.path.join(CONFIG["RESULTS_PATH"]+"_CHANNEL_"+channel)): shutil.rmtree(os.path.join(CONFIG["RESULTS_PATH"]+"_CHANNEL_"+channel))
	if os.path.exists(os.path.join(CONFIG["ROIS_PATH"]+"_CHANNEL_"+channel)): shutil.rmtree(os.path.join(CONFIG["ROIS_PATH"]+"_CHANNEL_"+channel))
	if not os.path.exists(os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"][:-4]+"_results")): os.mkdir(os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"][:-4]+"_results"))
	os.mkdir(os.path.join(CONFIG["RESULTS_PATH"]+"_CHANNEL_"+channel))
	os.mkdir(os.path.join(CONFIG["ROIS_PATH"]+"_CHANNEL_"+channel))

	# find and crop ROIs
	if os.path.exists(os.path.join(CONFIG["ROI_FILE"],"RoiSet.zip")): 
		print "using ROI file built by FIJI"
		CONFIG["ROI_FILE"] = os.path.join(CONFIG["ROI_FILE"], "RoiSet.zip")
		roim.runCommand("Open", CONFIG["ROI_FILE"])
		for roi in roim.getRoisAsArray():
			imp.setRoi(roi)
			imp_roi = imp.duplicate()
			imp_roi_list.append(imp_roi)
			roi_positions.append([roi.getBounds().x, roi.getBounds().y, roi.getBounds().width, roi.getBounds().height])
	else: 
		print "didn't find any ROI to crop"
		return
	# clean up
	imp.close()



	return imp_roi_list, roi_positions

def analyseRoi(imp_roi, title, CONFIG):
	print "title = ", title
	print "ROIS_PATH", os.path.join(CONFIG["ROIS_PATH"]+'_CHANNEL_'+channel+'_'+title+".tif")
	IJ.save(imp_roi, os.path.join(CONFIG["ROIS_PATH"]+'_CHANNEL_'+channel+'_'+title+".tif")) 

	if 1: # put at 0 to only save ROIs

		#print "Analysing Locus"
		CONFIG["boxSearchDev"] = 0
		lxcs, lycs, lzcs, limp, lis, lmis, lpixs, lcs = segmentLocus(imp_roi, CONFIG)
		#print "Analysing nucleus"
		nxcs, nycs, nimp, nis, npixs = segmentNucleus(imp_roi, lis, lpixs, CONFIG)

		if CONFIG["saveGraphicalOutput"]:
			print "Plotting centers"
			cimp =  plotCenters(imp_roi, nxcs, nycs, lxcs, lycs,lcs,lmis,nis)
			#showLocusHist(lxcs, lycs)

			nell_x, nell_y, nell_maj, nell_min, nell_angle = fitNucleoplasm(imp_roi, nxcs, nycs)
	
			w = imp_roi.getWidth()
			h = imp_roi.getHeight()
			nslices = imp_roi.getNSlices()
 
			CONFIG["MAIN_LOCK"].acquire()

			cimp.show()
			nimp.show()
			limp.show()
			imp_roi.show()
			
			IJ.run(imp_roi, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");
			IJ.run(limp, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");
			IJ.run(nimp, "Size...", "width="+str(w*CONFIG["MAGN"])+" height="+str(h*CONFIG["MAGN"])+" depth="+str(nslices)+" interpolation=None");
		
			IJ.run("Merge Channels...", "c1="+imp_roi.getTitle()+" c2="+limp.getTitle()+" c3="+nimp.getTitle()+" c4="+cimp.getTitle()+" create")
			imp_rgb = IJ.getImage()
			imp_rgb.hide()
			IJ.save(imp_rgb, os.path.join(CONFIG["RESULTS_PATH"]+'_CHANNEL_'+channel+'_'+title+"_colorMerge.tif"))
			imp_rgb.close()
			CONFIG["MAIN_LOCK"].release()

	# dump info
	# dump datas
	f = file(os.path.join(CONFIG["RESULTS_PATH"]+'_CHANNEL_'+channel+'_'+title+"_data.txt"), 'w') # results of the analysis
	f.write("Locus X (px)\tLocus Y (px)\tLocus Z (px)\tNucleus X (px)\tNucleus Y (px)\tIntensity ratios\tLocus MaxInt\tSNR_max\tSNR_mean\n")
	for n in range(len(lxcs)):
		#coef = 1.-(float(nis[n])/float(lis[n])) # old version doesn't work
		coef = lmis[n]/float(nis[n])
		#IJ.log("image = %d   lmis[n]=%d    float(nis[n])=%d" % (n, lmis[n], float(nis[n])))
		f.write("%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.3f\t%.3f\n" % (lxcs[n], lycs[n], lzcs[n], nxcs[n], nycs[n], coef, lmis[n],lcs['SNR_max'][n],lcs['SNR_mean'][n]))
	f.close()

	# dump several suppl parameters
	suppl = file(os.path.join(CONFIG["RESULTS_PATH"]+'_CHANNEL_'+channel+'_'+title+"_suppl.txt"), 'w') # confidence of spot detection ROI + area locus and nucleoplasm
	if CONFIG["CENTROID_METHOD"]=="center of mass":
		# print "saving center of mass style"
		suppl.write("SNR_max\tSNR_mean\txstd\tystd\tmaxSym\txsym\tysym\tIratio\tlocus area(px)\tnucleus area(px)\tCM maxValue\n")
		for n in range(len(lxcs)):
			coef = lmis[n]/float(nis[n])
			suppl.write("%.3f\t%.3f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % \
				(lcs['SNR_max'][n],lcs['SNR_mean'][n],lcs['lxstd'][n],lcs['lystd'][n],lcs['maxSym'][n],lcs['xsym'][n],lcs['ysym'][n],coef,lpixs[n],npixs[n],lmis[n]))
		suppl.close()
	elif CONFIG["CENTROID_METHOD"]=="gaussian fitting":
		# print "saving gaussian fitting style"
		suppl.write("SNR_max\tSNR_mean\txstd\tystd\tmaxSym\txsym\tysym\tIratio\tlocus area(px)\tnucleus area(px)\tCM maxValue\tGF maxValue\tgaussian refine\tgaussian residue\n")
		for n in range(len(lxcs)):
			coef = lmis[n]/float(nis[n])
			suppl.write("%.3f\t%.3f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % \
				(lcs['SNR_max'][n],lcs['SNR_mean'][n],lcs['lxstd'][n],lcs['lystd'][n],lcs['maxSym'][n],lcs['xsym'][n],lcs['ysym'][n],coef, \
				lpixs[n],npixs[n],lmis[n],lcs['gMaxVal'][n],lcs['gRefine'][n],lcs['gResidue'][n]))
		suppl.close()
	else: print "Warning Conf file not saved => Centroid method not detected"

#	# dump ellipse values
#	ellipse = file(os.path.join(CONFIG["RESULTS_PATH"], title+"_ellipse.txt"), 'w') # fitted ellipse of the nucleoplasm
#	ellipse.write("xCent\tyCent\tmajAxis\tminAxis\tAngle\n")
#	ellipse.write("%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n" % (nell_x, nell_y, nell_maj, nell_min, nell_angle))
#	ellipse.close()





def main():

	for channel in range(CONFIG["NBRE_CHANNELS"]):

		imp = loadImage(channel,CONFIG)

		imp_roi_list, roi_positions = cropROIs(imp,CONFIG,channel)

		type(imp_roi_list[0])


		# process and save each individual ROI
		print 'nrois = ',len(imp_roi_list)
		 
		thread_pool = []
		for r in range(len(imp_roi_list)):
			#if r!=105: continue
			#if r != 0: continue
			IJ.log("Processing ROI %d/%d" % (r+1, len(imp_roi_list)))
			title = "yeast_ROI%d" % r
	
			imp_roi = imp_roi_list
			# process each individual ROI
			if CONFIG["ENABLE_THREADING"]:		
				if len(thread_pool) < CONFIG["NCPUS"]:
					t = threading.Thread(target = analyseRoi, args = (imp_roi[r], title, CONFIG))
					t.start()
					thread_pool.append(t)
					while len(thread_pool) == CONFIG["NCPUS"]:
						time.sleep(0.1)
						for n in range(len(thread_pool)):
							if not thread_pool[n].isAlive():
								thread_pool.pop(n)
								break
			else:		
				analyseRoi(imp_roi[r], title, CONFIG)
	
		# dump settings
		settings = file(os.path.join(CONFIG["RESULTS_PATH"]+'_CHANNEL_'+channel+'_'+"condition_settings.txt"), 'w') # settings of imaging
		settings.write("pixelSize(nm)\tNbreChannels\n")
		settings.write("%.5f\t%r\n" % (CONFIG["pixelSize"],CONFIG["NBRE_CHANNELS"]))
		settings.close()

		# dump roi_positions
		rois = file(os.path.join(CONFIG["RESULTS_PATH"]+'_CHANNEL_'+channel+'_'+"roi_positions.txt"), 'w') # positions of the roi x, y, width, height
		rois.write("x(pix)\ty(pix)\twidth(pix)\theight(pix)\n")
		for ROIs in roi_positions:
			rois.write("%r\t%r\t%r\t%r\n" % (ROIs[0], ROIs[1], ROIs[2], ROIs[3]))
		rois.close()
	
		# make sure that all the threads are finished before proceeding with the rest of the analysis
		while len(thread_pool)>0:
			time.sleep(0.5)
			for n in range(len(thread_pool)):
				if not thread_pool[n].isAlive():
					thread_pool.pop(n)
					break
			
	IJ.run("Close All")
	print "done :)"
	IJ.log("done :)")
		



def serial_calling():

#	dc = DirectoryChooser("Select path to analyse...")
#	present_LOCAL_PATH = dc.getDirectory()
#	if present_LOCAL_PATH is None: raise Exception("No folder chosen...")

	od = OpenDialog("Choose multi-image file", None)
	srcDir = od.getDirectory()
	if srcDir is None:
	  # User canceled the dialog
	  return
	present_LOCAL_FILE = os.path.join(srcDir, od.getFileName())
		
	LOCAL_FILES = []
	loop = 1
	while not None == present_LOCAL_FILE: #and loop<15 :
		LOCAL_FILES.append(present_LOCAL_FILE)
		print "analysing : ", present_LOCAL_FILE

		od = OpenDialog("Choose multi-image file", None)
		srcDir = od.getDirectory()
		if srcDir is None:
		  	# User canceled the dialog
	  		break
		present_LOCAL_FILE = os.path.join(srcDir, od.getFileName())
		loop=loop+1
		#if loop==15: print "limite of 15 folders reached... beginning the analysis"


	for files in LOCAL_FILES:
		CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"] = os.path.split(files)
		print "Processing folder '%s' under '%s'..." % (CONFIG["LOCAL_PATH"], CONFIG["BASE_PATH"])

		CONFIG["BASE_IMAGE"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"])
		CONFIG["RESULTS_PATH"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"][:-4]+"_results", "results")
		CONFIG["ROIS_PATH"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"][:-4]+"_results", "rois")
		CONFIG["ROI_FILE"] = os.path.join(CONFIG["BASE_PATH"], CONFIG["LOCAL_PATH"])
		main()



#  generic code to handle jython exceptions and show them in error dialog
import ij, sys, traceback
try: 
    serial_calling()
except:
    exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
    info = traceback.format_exception(exceptionType, exceptionValue,exceptionTraceback)
    info = "".join(info)
    ij.IJ.error("HDPALM-jython error", info)
    raise


